# README #

Simple program simulating boxing match.
During the fight boxers take turns - turn sequence is as follows:
1. boxer1 attacks, boxer2 blocks,
2. if block was unsuccessful boxer2 loses HP points,
3. if boxer2 has any HP left fight continues,
3. boxer2 attacks, boxer1 blocks,
4. if block was unsuccessful boxer1 loses HP points,
5. if boxer1 has any HP left new turn begins.
Turn order is determined randomly right before fight starts.