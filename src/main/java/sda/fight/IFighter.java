package sda.fight;

public interface IFighter {
    boolean isAlive();

    FighterAttackActionType attack();

    FighterDefenceActionType defend();

    void decreaseHp(int strength);

    String getName();

    int getHp();

    int getStrength();
}
