package sda.fight;

public class AggressiveBoxer extends Boxer {

    int baseHp;

    public AggressiveBoxer(String name, int hp, IFighterStyle style, int strength) {
        super(name, hp, style, strength);
        this.baseHp = hp;
    }

    @Override
    public int getStrength() {
        int stengthToReturn = strength;
        if ( getHp() < baseHp / 2 ) {
            stengthToReturn += 5;
        }
        return stengthToReturn;
    }
}

