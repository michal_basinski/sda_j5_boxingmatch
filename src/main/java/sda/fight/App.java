package sda.fight;

public class App 
{
    public static void main( String[] args ) {

        IFighter boxer1 = new AggressiveBoxer("Mike Tyson", 200, IFighterStyle.BALANCED, 10);
        IFighter boxer2 = new Boxer("Andrzej Gołota", 220, IFighterStyle.BALANCED,7);
        IFightingMatch match = new BoxingMatch(boxer1, boxer2);
        match.fight();
    }
}
